# ecr-audit

## What does it do?

Creates an Excel report of specified ECR images that shows all vulnerabilities and how many images are currently stored in ECR per repo.

## How to Run
1. Authenticate with **aws-mfa** (https://gitlab.com/msts-enterprise/infrastructure/aws-tools)
2. Make sure Rancher Desktop is running
2. Run **docker compose up**

