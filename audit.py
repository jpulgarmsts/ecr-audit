import boto3, xlsxwriter
import datetime
from datetime import date

client = boto3.client('ecr')

# Get listing of all repos and sort them
nextToken = {}
repolist = []

while True:
    response = client.describe_repositories(registryId="434875166128", **nextToken)
    for entry in response["repositories"]:
        repolist.append(entry["repositoryName"])
    if "nextToken" in response:
        nextToken = { "nextToken": response["nextToken"] }
    else:
        break

repolist.sort()

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook("ECR-Audit_" + str(date.today()) + ".xlsx")
worksheet = workbook.add_worksheet()

# Add headers
my_headers = ["ECR Repo", "Critical", "High", "Medium", "Low", "Informational", "Undefined", "Image Count", "Date Pushed", "Scan Completed"]

for col_num, data in enumerate(my_headers):
    worksheet.write(0, col_num, data)

# Start from the first cell. Rows and columns are zero indexed.
row = 1
col = 0

# Get latest image from each repo
for repo in repolist:
    
    date_string = '2010-01-01 11:11:11.001111 +0530' # old date so it will less than on first comparison
    latest_date = datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S.%f %z')
    image_count = 0
    latest_image = ""
    nextToken = {}
    while True:
        response = client.describe_images(repositoryName=repo, **nextToken)
        for entry in response["imageDetails"]:
            image_count = image_count + 1
            if entry["imagePushedAt"] > latest_date:
                latest_date = entry["imagePushedAt"]
                latest_image = entry
        if "nextToken" in response:
            nextToken = { "nextToken": response["nextToken"] }
        else:
            break

    if (latest_image != "" and "imageScanStatus" in latest_image and latest_image["imageScanStatus"]["status"] == "COMPLETE"):
        print(latest_image["repositoryName"])
        worksheet.write(row, col, latest_image["repositoryName"])
        if "CRITICAL" in latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]:
            worksheet.write(row, col + 1, int(latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]["CRITICAL"]))
        else:
            worksheet.write(row, col + 1, 0)
        if "HIGH" in latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]:
            worksheet.write(row, col + 2, int(latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]["HIGH"]))
        else:
            worksheet.write(row, col + 2, 0)
        if "MEDIUM" in latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]:
            worksheet.write(row, col + 3, int(latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]["MEDIUM"]))
        else:
            worksheet.write(row, col + 3, 0)
        if "LOW" in latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]:
            worksheet.write(row, col + 4, int(latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]["LOW"]))
        else:
            worksheet.write(row, col + 4, 0)
        if "INFORMATIONAL" in latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]:
            worksheet.write(row, col + 5, int(latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]["INFORMATIONAL"]))
        else:
            worksheet.write(row, col + 5, 0)
        if "UNDEFINED" in latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]:
            worksheet.write(row, col + 6, int(latest_image["imageScanFindingsSummary"]["findingSeverityCounts"]["UNDEFINED"]))
        else:
            worksheet.write(row, col + 6, 0)
        worksheet.write(row, col + 7, image_count)
        worksheet.write(row, col + 8, latest_date.strftime("%Y-%m-%d"))
        worksheet.write(row, col + 9, latest_image["imageScanFindingsSummary"]["imageScanCompletedAt"].strftime("%Y-%m-%d"))
        row += 1

workbook.close()
